
# Changelog for ShareLatex Wrapper Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.1.0-SNAPSHOT] -2021-11-29

- Ported to git
	
## [v1.0.0] -2016-04-11

- First release
